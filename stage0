#!/bin/bash

source ./config

# Checking values with no defaults
if [ -z "$DEVICE" ]; 
then 
    echo "[Error] Please enter valid device"; 
    exit 
fi

# checking if parted is installed
if ! command -v parted &> /dev/null
then
    echo "[Error] parted not installing"
    exit
fi

# GPT for EFI
parted $DEVICE --script mklabel gpt

# Boot partition - vfat 512Mb
parted $DEVICE --script --align=optimal mkpart primary 1MiB 513Mib
parted $DEVICE --script set 1 boot on

# Root partition - 100% space after boot partition
parted $DEVICE --script --align=optimal mkpart primary 513MiB 100%

# Creating file system and set label for boot
mkfs.fat -F32 -n EFI $BOOT_PARTITION
fatlabel $BOOT_PARTITION $BOOT_LABEL

# Creating file system for root
mkfs.btrfs -f -L $ROOT_LABEL $ROOT_PARTITION

# mount root partition to /mnt and create subvolumes
mount $ROOT_PARTITION /mnt

btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@opt
btrfs subvolume create /mnt/@tmp
btrfs subvolume create /mnt/@var
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@root
if [ $USE_SWAP -eq 1 ];
then
    btrfs subvolume create /mnt/@swap
fi
btrfs subvolume create /mnt/@snapshots

# Unmount everything 
umount -R /mnt

# mount root first to /mnt with options defined in config script
mount -o $BTRFS_OPTS,subvol=@ $ROOT_PARTITION /mnt

# create directories for home/boot/snapshots
mkdir -p /mnt/{opt,tmp,var,home,root,.snapshots,boot/efi}

if [ $USE_SWAP -eq 1 ];
then
    mkdir /mnt/swap
fi

# mount home/snapshots/boot
mount $BOOT_PARTITION /mnt/boot/efi
mount -o $BTRFS_OPTS,subvol=@opt $ROOT_PARTITION /mnt/opt
mount -o $BTRFS_OPTS,subvol=@tmp $ROOT_PARTITION /mnt/tmp
mount -o $BTRFS_OPTS,subvol=@var $ROOT_PARTITION /mnt/var
mount -o $BTRFS_OPTS,subvol=@home $ROOT_PARTITION /mnt/home
mount -o $BTRFS_OPTS,subvol=@root $ROOT_PARTITION /mnt/root
if [ $USE_SWAP -eq 1 ];
then
    mount -o $BTRFS_OPTS,subvol=@swap $ROOT_PARTITION /mnt/swap
fi
mount -o $BTRFS_OPTS,subvol=@snapshots $ROOT_PARTITION /mnt/.snapshots

# install base system with xbps-install
XBPS_ARCH=$ARCH xbps-install -Sy -r /mnt -R "$REPO" base-system btrfs-progs

# mount sys/dev/proc/run for chroot process
for dir in dev proc sys run;
    do mount --rbind /$dir /mnt/$dir; mount --make-rslave /mnt/$dir;
done

# copy resolve.config and other scripts to root  
cp /etc/resolv.conf /mnt/etc
cp ./stage1 /mnt/stage1
cp ./config /mnt/config
cp ./custom /mnt/custom

# copy custom and config into the root folder fore post-reboot instllation

cp ./custom /mnt/root/custom
cp ./config /mnt/root/config

# !!! Here the place, where stage1 script will be executed as part of chroot process !!!
PS1='chroot # ' chroot /mnt /bin/bash +x ./stage1

# remove scripts after installation
rm -f /mnt/stage1 /mnt/config
